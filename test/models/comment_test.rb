require 'test_helper'

class CommentTest < ActiveSupport::TestCase
  setup do
    @article = articles(:one)
  end

  test 'valid comment' do
    comment = Comment.new(commenter: 'me', body: 'nice!', article: @article)
    assert comment.valid?
  end

  test 'invalid without article' do
    comment = Comment.new(commenter: 'me', body: 'nice!')
    refute comment.valid?, 'comment is invalid without an article'
    assert_not_nil comment.errors[:article]
  end

  test 'invalid without commenter' do
    comment = Comment.new(body: 'nice!', article: @article)
    refute comment.valid?, 'comment is invalid without a commenter'
    assert_not_nil comment.errors[:commenter]
  end
  
end
