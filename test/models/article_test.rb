require 'test_helper'

class ArticleTest < ActiveSupport::TestCase
  test 'valid article' do
    article = Article.new(title: 'Hello', text: 'World')
    assert article.valid?
  end

  test 'invalid without title' do
    article = Article.new(text: 'World')
    refute article.valid?, 'article is invalid without a title'
    assert_not_nil article.errors[:title]
  end

  test 'invalid with less than 5 chars title' do
    article = Article.new(title: 'Hi', text: 'World')
    refute article.valid?, 'article is invalid with less than 5 chars title'
    assert_not_nil article.errors[:title]
  end

  test 'invalid without text' do
    article = Article.new(title: 'World')
    refute article.valid?, 'article is invalid without a text'
    assert_not_nil article.errors[:text]
  end
  
end
